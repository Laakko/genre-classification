# genre-classification

Final project for "Principles of Machine Learning" course:  
Identifying music genres of songs using Machine Learning classification methods.   
Training data set is a custom subset of Million Song Database and training labels
are gotten from AllMusic.com. The features contain 3 main components of music: timbre, pitch and rhythm. Genres corresponding to the 10 labels are:  
1   'Pop_Rock'  
2   'Electronic'  
3   'Rap'  
4   'Jazz'  
5   'Latin'  
6   'RnB'  
7   'International'  
8   'Country'  
9   'Reggae'  
10  'Blues  